import axios from "../lib/http";
const BASE_PATH = "/auth";

class AuthService {
  login(user) {
    console.log(axios);
    return axios
      .post(BASE_PATH + "/signin", {
        email: user.email,
        password: user.password,
      })
      .then((response) => {
        if (response.data.accessToken) {
          localStorage.setItem("user", JSON.stringify(response.data));
        }

        return response.data;
      });
  }

  logout() {
    localStorage.removeItem("user");
  }

  register(user) {
    return axios.post(BASE_PATH + "/signup", {
      firstName: user.firstName,
      lastName: user.lastName,
      email: user.email,
      password: user.password,
    });
  }
}

export default new AuthService();

import axios from "../lib/http";
import authHeader from "./auth-header";
const BASE_PATH = "/users";

class UserService {
  getUsers() {
    return axios.get(BASE_PATH, { headers: authHeader() });
  }

  getMe() {
    return axios.get(BASE_PATH + "/me", { headers: authHeader() });
  }

  updateProfile(firstName, lastName, dob, address, phone) {
    return axios.post(
      BASE_PATH + "/me",
      { firstName, lastName, dob, address, phone },
      { headers: authHeader() }
    );
  }
}

export default new UserService();

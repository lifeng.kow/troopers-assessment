require("dotenv").config();

const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const app = express();

var corsOptions = {
  origin: process.env.originUrl,
};

// before-middleware
app.use(cors(corsOptions));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

const db = require("./src/models");
// always resync db when restart, not for production
// db.sequelize.sync({ force: true }).then(() => {
//   console.log("Drop and re-sync db.");
// });

// Auth Routes
const authRoutes = require("./src/routes/auth.route.js");
app.use("/api/auth", authRoutes);

// User Routes
const userRoutes = require("./src/routes/user.route.js");
app.use("/api/users", userRoutes);

// catch all other routes as not found
const HttpError = require("./src/lib/types/httpError");
app.get("*", function (req, res) {
  throw new HttpError(404, "not found");
});

// after-middleware
const { httpErrorMiddleware } = require("./src/middlewares");
app.use(httpErrorMiddleware);

// set port, listen for requests
const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});

const db = require("../models");
const config = require("../config/auth.config");
const User = db.user;
const HttpError = require("../lib/types/httpError");

var jwt = require("jsonwebtoken");
var bcrypt = require("bcryptjs");

exports.signup = async (req, res, next) => {
  const user = await User.findOne({
    where: {
      email: req.body.email,
    },
  });

  if (user) {
    return next(new HttpError(400, "Email is already in use!"));
  }

  await User.create({
    firstName: req.body.firstName,
    lastName: req.body.lastName,
    email: req.body.email,
    password: bcrypt.hashSync(req.body.password, 8),
  });

  return res.status(201).json({ message: "User registered successfully!" });
};

exports.signin = async (req, res, next) => {
  const user = await User.findOne({
    where: {
      email: req.body.email,
    },
  });

  if (!user) {
    return next(new HttpError(404, "User not found!"));
  }

  var passwordIsValid = bcrypt.compareSync(req.body.password, user.password);

  if (!passwordIsValid) {
    return next(new HttpError(401, "Invalid Password!"));
  }

  const token = jwt.sign({ id: user.id }, config.secret, {
    algorithm: "HS256",
    allowInsecureKeySizes: true,
    expiresIn: 86400, // 24 hours
  });

  return res.status(200).send({
    id: user.id,
    email: user.email,
    firstName: user.firstName,
    lastName: user.lastName,
    accessToken: token,
  });
};

const db = require("../models");
const User = db.user;
const HttpError = require("../lib/types/httpError");

exports.allUser = async (req, res, next) => {
  const users = await User.findAll();

  return res.status(200).json(users);
};

exports.me = async (req, res, next) => {
  const user = await User.findByPk(req.userId);

  if (!user) {
    return next(new HttpError(404, "User not found!"));
  }

  return res.status(200).json(user);
};

exports.updateMe = async (req, res, next) => {
  const user = await User.findByPk(req.userId);

  if (!user) {
    return next(new HttpError(404, "User not found!"));
  }

  user.firstName = req.body.firstName;
  user.lastName = req.body.lastName;
  user.dob = req.body.dob;
  user.address = req.body.address;
  user.phone = req.body.phone;
  await user.save();

  return res.status(200).json({ user, message: "Profile saved successfully" });
};

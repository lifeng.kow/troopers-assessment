const express = require("express");
const router = express.Router();
const userController = require("../controllers/user.controller.js");
const { authJwt } = require("../middlewares");

router.use(function (req, res, next) {
  res.header(
    "Access-Control-Allow-Headers",
    "Authorization, Origin, Content-Type, Accept"
  );
  next();
});

router.get("/", [authJwt.verifyToken], userController.allUser);

router.get("/me", [authJwt.verifyToken], userController.me);
router.post("/me", [authJwt.verifyToken], userController.updateMe);

module.exports = router;

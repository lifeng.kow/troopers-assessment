const jwt = require("jsonwebtoken");
const config = require("../config/auth.config.js");
const HttpError = require("../lib/types/httpError");

verifyToken = (req, res, next) => {
  let token = req.headers["authorization"];

  if (!token) {
    throw new HttpError(403, "No token provided!");
  }

  jwt.verify(token, config.secret, (err, decoded) => {
    if (err) {
      throw new HttpError(401, "Unauthorized!");
    }
    req.userId = decoded.id;
    next();
  });
};

const authJwt = {
  verifyToken: verifyToken,
};

module.exports = authJwt;

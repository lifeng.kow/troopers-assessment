const authJwt = require("./authJwt.middleware");
const httpErrorMiddleware = require("./httpError.middleware");

module.exports = {
  authJwt,
  httpErrorMiddleware,
};

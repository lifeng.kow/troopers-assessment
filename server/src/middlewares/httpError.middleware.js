const jwt = require("jsonwebtoken");
const config = require("../config/auth.config.js");
const HttpError = require("../lib/types/httpError.js");

httpErrorMiddleware = (err, req, res, next) => {
  res
    .status(err.statusCode || 500)
    .json({ message: err.message || "Internal server error" });

  next();
};

module.exports = httpErrorMiddleware;
